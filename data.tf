resource random_password rcon_password {
  length = 15
}

output rcon_password {
  value = random_password.rcon_password.result
  sensitive = true
}