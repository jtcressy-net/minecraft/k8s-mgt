locals {
  sponge_plugin_urls = [
    "https://ci.lucko.me/job/LuckPerms/1053/artifact/sponge/build/libs/LuckPerms-Sponge-5.0.139.jar"
  ]
}

resource kubernetes_config_map sponge-configs {
  metadata {
    name = "sponge-configs"
    namespace = kubernetes_namespace.minecraft.metadata.0.name
  }
  binary_data = {}
  data = {
    "plugin_script.sh" = <<-EOT
    #!/bin/ash
    for i in $${PLUGINS//,/ }
    do
      EFFECTIVE_PLUGIN_URL=$(curl -Ls -o /dev/null -w %%{url_effective} $i)
      case "X$EFFECTIVE_PLUGIN_URL" in
        X[Hh][Tt][Tt][Pp]*.jar)
          echo "Downloading plugin via HTTP"
          echo "  from $EFFECTIVE_PLUGIN_URL ..."
          if ! curl -sSL -o /tmp/$${EFFECTIVE_PLUGIN_URL##*/} $EFFECTIVE_PLUGIN_URL; then
            echo "ERROR: failed to download from $EFFECTIVE_PLUGIN_URL to /tmp/$${EFFECTIVE_PLUGIN_URL##*/}"
            exit 2
          fi

          mv /tmp/$${EFFECTIVE_PLUGIN_URL##*/} "/plugins/$${EFFECTIVE_PLUGIN_URL##*/}"
          rm -f /tmp/$${EFFECTIVE_PLUGIN_URL##*/}
          ;;
        *)
          echo "Invalid URL given for plugin list: Must be HTTP or HTTPS and a JAR file"
          ;;
      esac
    done
    EOT
    "global.conf" = <<-EOT
    sponge {
      bungeecord {
        ip-forwarding = true
      }
      modules {
        bungeecord = true
      }
    }
    EOT
    #"tracker.conf" = ""
    #"custom_data.conf" = ""
  }
}