provider google {
  project = "jtcressy-net-235001"
}

provider kubernetes {
  version = "~> 1.11"
  load_config_file = true
}

provider helm {
  version = "~> 1.1"
  kubernetes {
    load_config_file = true
  }
}