//resource random_password redis-password {
//  length = 32
//}
//
//resource kubernetes_secret redis-password {
//  metadata {
//    name = "redis-password"
//    namespace = kubernetes_namespace.minecraft.metadata.0.name
//  }
//  data = {
//    password = random_password.redis-password.result
//  }
//}
//
//resource helm_release bungee-redis {
//  name = "bungee"
//  chart = "redis"
//  repository = data.helm_repository.bitnami.metadata[0].url
//  version = ""
//  namespace = kubernetes_namespace.minecraft.metadata.0.name
//  values = [yamlencode({
//    existingSecret = kubernetes_secret.redis-password.metadata.0.name
//    existingSecretPasswordKey = "password"
//  })]
//}