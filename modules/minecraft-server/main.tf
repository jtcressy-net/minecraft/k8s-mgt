locals {
  metadata_overrides = {
    labels = merge(var.metadata.labels, {
      "mc.jtcressy.net/server" = var.metadata.name
    })
    annotations = merge(var.metadata.annotations, {

    })
  }
  envs = merge(var.extraEnvs, {
    EULA                         = var.eula ? "TRUE" : "FALSE"
    TYPE                         = var.type
    VERSION                      = var.mc_version
    DIFFICULTY                   = var.minecraft.difficulty
    WHITELIST                    = jsonencode(var.minecraft.whitelist)
    OPS                          = jsonencode(var.minecraft.ops)
    ICON                         = var.minecraft.icon
    MAX_PLAYERS                  = var.minecraft.max_players
    MAX_WORLD_SIZE               = var.minecraft.max_world_size
    ALLOW_NETHER                 = var.minecraft.allow_nether
    ANNOUNCE_PLAYER_ACHIEVEMENTS = var.minecraft.announce_player_achievements
    ENABLE_COMMAND_BLOCK         = var.minecraft.enable_command_block
    FORCE_gameMode               = var.minecraft.force_game_mode
    FORCE_REDOWNLOAD             = var.force_re_download
    GENERATE_STRUCTURES          = var.minecraft.generate_structures
    HARDCORE                     = var.minecraft.hardcore
    MAX_BUILD_HEIGHT             = var.minecraft.max_build_height
    MAX_TICK_TIME                = var.minecraft.max_tick_time
    SPAWN_ANIMALS                = var.minecraft.spawn_animals
    SPAWN_MONSTERS               = var.minecraft.spawn_monsters
    SPAWN_NPCS                   = var.minecraft.spawn_npcs
    VIEW_DISTANCE                = var.minecraft.view_distance
    MODE                         = var.minecraft.game_mode
    MOTD                         = var.minecraft.motd
    PVP                          = var.minecraft.pvp
    SEED                         = var.level_generator.seed
    LEVEL_TYPE                   = var.level_generator.level_type
    GENERATOR_SETTINGS           = var.level_generator.generator_settings
    LEVEL                        = var.world.name
    WORLD                        = var.world.url
    MODPACK                      = var.download_modpack_url
    REMOVE_OLD_MODS              = var.remove_old_mods
    ONLINE_MODE                  = var.minecraft.online_mode
    ENABLE_QUERY                 = "true"
    QUERY_PORT                   = "25565"
  })
}

resource kubernetes_service clusterip {
  metadata {
    name        = var.metadata.name
    namespace   = var.metadata.namespace
    labels      = merge(var.metadata.labels, local.metadata_overrides.labels)
    annotations = merge(var.metadata.annotations, local.metadata_overrides.annotations)
  }
  spec {
    type = "ClusterIP"
    port {
      port     = 25565
      protocol = "TCP"
      name     = "minecraft"
    }
    publish_not_ready_addresses = true
    selector = {
      "mc.jtcressy.net/server" = var.metadata.name
    }
  }
}
resource kubernetes_service headless {
  metadata {
    name        = "${var.metadata.name}-headless"
    namespace   = var.metadata.namespace
    labels      = merge(var.metadata.labels, local.metadata_overrides.labels)
    annotations = merge(var.metadata.annotations, local.metadata_overrides.annotations)
  }
  spec {
    type       = "ClusterIP"
    cluster_ip = "None"
    port {
      port     = 25565
      protocol = "TCP"
      name     = "minecraft"
    }
    publish_not_ready_addresses = true
    selector = {
      "mc.jtcressy.net/server" = var.metadata.name
    }
  }
}

resource kubernetes_stateful_set server {
  metadata {
    name        = var.metadata.name
    namespace   = var.metadata.namespace
    labels      = merge(var.metadata.labels, local.metadata_overrides.labels)
    annotations = merge(var.metadata.annotations, local.metadata_overrides.annotations)
  }
  spec {
    pod_management_policy = "Parallel"
    service_name          = kubernetes_service.headless.metadata[0].name
    selector {
      match_labels = {
        "mc.jtcressy.net/server" = var.metadata.name
      }
    }
    template {
      metadata {
        labels      = local.metadata_overrides.labels
        annotations = local.metadata_overrides.annotations
      }
      spec {
        security_context {
          run_as_user = 1000
          fs_group    = 2000
        }
        container {
          name              = "minecraft"
          image             = "${var.image.name}:${var.image.tag}"
          image_pull_policy = var.image.pull_policy
          resources {
            limits {
              cpu = var.resources.cpu
              memory = var.resources.memory
            }
            requests {
              cpu = var.resources.cpu
              memory = var.resources.memory
            }
          }
          readiness_probe {
            exec {
              command = [
                "mcstatus",
                "localhost:25565",
                "status"
              ]
            }
            initial_delay_seconds = 30
            period_seconds        = 5
            failure_threshold     = 10
            success_threshold     = 1
            timeout_seconds       = 1
          }
//          liveness_probe {
//            exec {
//              command = [
//                "mcstatus",
//                "localhost:25565",
//                "status"
//              ]
//            }
//            initial_delay_seconds = 300
//            period_seconds        = 5
//            failure_threshold     = 10
//            success_threshold     = 1
//            timeout_seconds       = 1
//          }
          dynamic env {
            for_each = var.type == "FORGE" ? var.forge_version != "" ? {
              FORGEVERSION = var.forge_version
              } : var.forge_installer_url != "" ? {
              FORGE_INSTALLER_URL = var.forge_installer_url
            } : {} : {}
            content {
              name  = env.key
              value = env.value
            }
          }
          dynamic env {
            for_each = var.type == "SPIGOT" && var.spigot_download_url != "" ? {
              SPIGOT_DOWNLOAD_URL = var.spigot_download_url
            } : {}
            content {
              name  = env.key
              value = env.value
            }
          }
          dynamic env {
            for_each = var.type == "BUKKIT" && var.bukkit_download_url != "" ? {
              BUKKIT_DOWNLOAD_URL = var.bukkit_download_url
            } : {}
            content {
              name  = env.key
              value = env.value
            }
          }
          dynamic env {
            for_each = var.type == "PAPER" && var.paper_download_url != "" ? {
              PAPER_DOWNLOAD_URL = var.paper_download_url
            } : {}
            content {
              name  = env.key
              value = env.value
            }
          }
          dynamic env {
            for_each = var.type == "FTB" ? {
              FTB_SERVER_MOD      = var.ftb_server_mod
              FTB_LEGACYJAVAFIXER = var.ftb_legacy_java_fixer
            } : {}
            content {
              name  = env.key
              value = env.value
            }
          }
          dynamic env {
            for_each = local.envs
            content {
              name  = env.key
              value = tostring(env.value)
            }
          }
          env {
            name  = "JVM_OPTS"
            value = var.jvm.options
          }
          env {
            name  = "JVM_XX_OPTS"
            value = var.jvm.xx_options
          }
          env {
            name  = "MEMORY"
            value = var.resources.memory
          }
          port {
            name           = "minecraft"
            container_port = 25565
            protocol       = "TCP"
          }
          dynamic env {
            for_each = var.rcon.enabled ? [true] : []
            content {
              name  = "ENABLE_RCON"
              value = "true"
            }
          }
          dynamic env {
            for_each = var.rcon.enabled ? [var.rcon.secretKeyRef] : []
            content {
              name = "RCON_PASSWORD"
              value_from {
                secret_key_ref {
                  name = env.value.name
                  key = env.value.key
                }
              }
            }
          }
          dynamic port {
            for_each = var.rcon.enabled ? [var.rcon.port] : []
            content {
              container_port = port.value
              name           = "rcon"
              protocol       = "TCP"
            }
          }
          volume_mount {
            mount_path = "/data"
            name       = "datadir"
          }
          volume_mount {
            mount_path = "/configs"
            name       = "configs"
          }
          volume_mount {
            mount_path = "/mods"
            name       = "mods"
          }
          volume_mount {
            mount_path = "/plugins"
            name       = "plugins"
          }
        }
        #node_selector {}
        #affinity {}
        #toleration {}
        volume {
          name      = "mods"
          empty_dir {}
        }
        volume {
          name      = "plugins"
          empty_dir {}
        }
        volume {
          name = "configs"
          config_map {
            name = kubernetes_config_map.configs.metadata.0.name
          }
        }
        dynamic volume {
          for_each = var.persistence.enabled ? [] : [var.persistence]
          content {
            name = "datadir"
            empty_dir {}
          }
        }
      }
    }
    update_strategy {
      type = "RollingUpdate"
    }
    dynamic volume_claim_template {
      for_each = var.persistence.enabled ? [var.persistence.storage_size] : []
      content {
        metadata {
          name = "datadir"
        }
        spec {
          access_modes = ["ReadWriteOnce"]
          resources {
            requests = {
              storage = volume_claim_template.value
            }
          }
        }
      }
    }
  }
}

resource kubernetes_config_map configs {
  metadata {
    name        = "${var.metadata.name}-configs"
    namespace   = var.metadata.namespace
    labels      = merge(var.metadata.labels, local.metadata_overrides.labels)
    annotations = merge(var.metadata.annotations, local.metadata_overrides.annotations)
  }
  data = var.configs
}