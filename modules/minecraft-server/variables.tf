variable metadata {
  type = object({
    name        = string
    namespace   = string
    labels      = map(string)
    annotations = map(string)
  })
}

variable image {
  default = {
    name = "itzg/minecraft-server"
    tag = "latest"
    pull_policy = "Always"
  }
  type = object({
    name = string
    tag = string
    pull_policy = string
  })
}

variable resources {
  type = object({
    cpu    = string
    memory = string
  })
}

variable extraEnvs {
  default = {}
  type = map(any)
}

variable configs {
  default = {}
  type = map(string)
}

variable eula {
  default = false
}
variable mc_version {
  description = "One of: LATEST, SNAPSHOT, or a specific version (ie: 1.7.9)"
  default     = "1.15.2"
}
variable type {
  description = "This can be one of VANILLA, FORGE, SPIGOT, BUKKIT, PAPER, FTB, SPONGEVANILLA, TUINITY, MAGMA"
  default     = "VANILLA"
  type        = string
}
variable forge_version { default = "" }
variable sponge_version { default = "" }
variable forge_installer_url { default = "" }
variable bukkit_download_url { default = "" }
variable spigot_download_url { default = "" }
variable paper_download_url { default = "" }
variable ftb_server_mod { default = "" }
variable ftb_legacy_java_fixer { default = "" }

variable minecraft {
  default = {
    difficulty                   = "easy"
    whitelist                    = []
    ops                          = []
    icon                         = ""
    max_players                  = 20
    max_world_size               = 10000
    max_build_height             = 256
    max_tick_time                = 60000
    allow_nether                 = true
    announce_player_achievements = true
    enable_command_block         = true
    force_game_mode              = false
    generate_structures          = true
    hardcore                     = false
    spawn_animals                = true
    spawn_monsters               = true
    spawn_npcs                   = true
    view_distance                = 10
    game_mode                    = "survival"
    motd                         = "A Minecraft Server on Kubernetes!"
    pvp                          = false
    online_mode                  = true
  }
  type = object({
    difficulty                   = string
    whitelist                    = list(string)
    ops                          = list(string)
    icon                         = string
    max_players                  = number
    max_world_size               = number
    max_build_height             = number
    max_tick_time                = number
    allow_nether                 = bool
    announce_player_achievements = bool
    enable_command_block         = bool
    force_game_mode              = bool
    generate_structures          = bool
    hardcore                     = bool
    spawn_animals                = bool
    spawn_monsters               = bool
    spawn_npcs                   = bool
    view_distance                = number
    game_mode                    = string
    motd                         = string
    pvp                          = bool
    online_mode                  = bool
  })
}
variable resource_pack {
  default = {
    url  = ""
    sha1 = ""
  }
  type = object({
    url  = string
    sha1 = string
  })
}
variable level_generator {
  default = {
    level_type         = "DEFAULT"
    generator_settings = ""
    seed               = ""
  }
  type = object({
    level_type         = string
    generator_settings = string
    seed               = string
  })
}
variable world {
  default = {
    name = "world"
    url  = ""
  }
  type = object({
    name = string
    url  = string
  })
}

variable force_re_download {
  default = false
}

variable download_modpack_url {
  default = ""
}

variable remove_old_mods {
  default = false
}

variable jvm {
  default = {
    options    = ""
    xx_options = ""
  }
  type = object({
    options    = string
    xx_options = string
  })
}

variable rcon {
  default = {
    enabled = false
    port    = 25566
    secretKeyRef = {
      name = ""
      key  = ""
    }
  }
  type = object({
    enabled = bool
    port    = number
    secretKeyRef = object({
      name = string
      key  = string
    })
  })
}

variable persistence {
  default = {
    enabled      = true
    storage_size = "1Gi"
  }
  type = object({
    enabled      = bool
    storage_size = string
  })
}