locals {
  lobby_specific_overrides = {
    MAX_PLAYERS = "200"
    MAX_WORLD_SIZE = "1000"
    ALLOW_NETHER = "false"
    ANNOUNCE_PLAYER_ACHIEVEMENTS = "false"
    GENERATE_STRUCTURES = "false"
    SPAWN_ANIMALS = "false"
    SPAWN_MONSTERS = "false"
    SPAWN_NPCS = "false"
  }
}

resource random_pet lobby-1-12 {
  keepers = {
    config_data = base64sha512(jsonencode(kubernetes_config_map.sponge-configs.data))
    config_raw_data = base64sha512(jsonencode(kubernetes_config_map.sponge-configs.data))
  }
  prefix = "lobby-1-12"
  separator = "-"
}

resource kubernetes_service lobby-1-12 {
  metadata {
    name = "lobby-1-12"
    namespace = kubernetes_namespace.minecraft.metadata.0.name
    labels = {
      "mc.jtcressy.net/server" = "lobby-1-12"
    }
  }
  spec {
    type = "ClusterIP"
    cluster_ip = "None"
    port {
      port = 25565
      protocol = "TCP"
      name = "minecraft"
    }
    selector = {
      "mc.jtcressy.net/server" = "lobby-1-12"
    }
  }
}

resource kubernetes_stateful_set lobby-1-12 {
  metadata {
    name = random_pet.lobby-1-12.id
    namespace = kubernetes_namespace.minecraft.metadata.0.name
    labels = {
      "mc.jtcressy.net/server" = "lobby-1-12"
    }
  }
  spec {
    service_name = kubernetes_service.lobby-1-12.metadata.0.name
    replicas = 1
    selector {
      match_labels = {
        "mc.jtcressy.net/server" = "lobby-1-12"
      }
    }
    template {
      metadata {
        labels = {
          "mc.jtcressy.net/server" = "lobby-1-12"
        }
        annotations = {
          "ad.datadoghq.com/minecraft.check_names" = jsonencode(["jmx"])
          "ad.datadoghq.com/minecraft.init_configs" = "[{}]"
          "ad.datadoghq.com/minecraft.instances" = jsonencode([
            {
              host = "%%host%%"
              port = "7091"
            }
          ])
        }
      }
      spec {
        security_context {
          run_as_user = 1000
          fs_group = 2000
        }
        container {
          name = "minecraft"
          image = "itzg/minecraft-server:latest"
          image_pull_policy = "Always"
          resources {
            requests {
              cpu = "250m"
              memory = "600Mi"
            }
            limits {
              cpu = "1500m"
              memory = "600Mi"
            }
          }
          env {
            name = "MEMORY"
            value = "512M"
          }
          port {
            container_port = 25565
            name = "minecraft"
            protocol = "TCP"
          }
          readiness_probe {
            exec {
              command = [
                "mcstatus",
                "localhost:25565",
                "status"
              ]
            }
            initial_delay_seconds = 30
            period_seconds        = 5
            failure_threshold     = 10
            success_threshold     = 1
            timeout_seconds       = 1
          }
          env {
            name = "ENABLE_JMX"
            value = "true"
          }
          port {
            name = "jmx"
            container_port = 7091
          }
          dynamic env { # Basic server version settings
            for_each = {
              EULA = "TRUE"
              VERSION = "1.12.2"
              TYPE = "SPONGEVANILLA"
              SPONGEVERSION = "1.12.2-7.2.0"
            }
            content {
              name = env.key
              value = env.value
            }
          }
          dynamic env { # World stuff
            for_each = {
              LEVEL = "world"
              WORLD = "https://static.planetminecraft.com/files/resource_media/schematic/1838/construction212-world-1537200078.zip"
            }
            content {
              name = env.key
              value = env.value
            }
          }
          dynamic env {
            for_each = local.lobby_specific_overrides
            content {
              name = env.key
              value = env.value
            }
          }
          volume_mount {
            mount_path = "/data"
            name = "data"
          }
        }
        volume {
          name = "data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.minecraft-lobby-1-12.metadata.0.name
          }
        }
      }
    }
  }
}

resource kubernetes_persistent_volume_claim minecraft-lobby-1-12 {
  metadata {
    name = "minecraft-lobby-1-12"
    namespace = kubernetes_namespace.minecraft.metadata.0.name
    annotations = {
      "gcs.csi.ofek.dev/bucket" = "jtcressy-net-minecraft-lobby-1-12"
      "gcs.csi.ofek.dev/gid" = "2000"
      "gcs.csi.ofek.dev/uid" = "1000"
    }
  }
  spec {
    access_modes = [
      "ReadWriteMany"
    ]
    resources {
      requests = {
        storage = "5Gi"
      }
    }
    storage_class_name = kubernetes_storage_class.csi-gcs.metadata.0.name
  }
  lifecycle {
    prevent_destroy = true
  }
}