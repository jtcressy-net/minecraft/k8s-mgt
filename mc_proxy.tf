locals {
  proxy_plugin_urls = [
    "https://ci.lucko.me/job/LuckPerms/1053/artifact/bungee/build/libs/LuckPerms-Bungee-5.0.139.jar"
  ]
}

resource random_password forwarding-secret {
  length = 15
}

resource random_uuid metrics {}

resource kubernetes_config_map proxy-config {
  metadata {
    name = "proxy-config"
    namespace = kubernetes_namespace.minecraft.metadata.0.name
  }
  data = {
    "config.yml" = yamlencode({ # Bungeecord config
      ip_forward = true
      forge_support = true
      network_compression_threshold = 256
      stats = random_uuid.metrics.result
      permissions = {
        default = [
          "bungeecord.command.server",
          "bungeecord.command.list"
        ]
        admin = [
          "bungeecord.command.alert",
          "bungeecord.command.end",
          "bungeecord.command.ip",
          "bungeecord.command.reload"
        ]
      }
      groups = {
        admin = [
          "jtcressy"
        ]
      }
      servers = {
        lobby-1-12 = {
          address = "lobby-1-12:25565"
          restricted = false
          motd = "&1Lobby Server - 1.12"
        }
      }
      timeout = 30000
      listeners = [
        {
          motd = " &6jtcressy.net &dMinecraft Network &r- &8Server: $${POD_NAME}"
          priorities = [
//            "lobby-1-15",
            "lobby-1-12"
          ]
          host = "0.0.0.0:25565"
          max_players = 50000
          force_default_server = false
          tab_size = 60
          forced_hosts = {
            "lobby.mc.jtcressy.net" = "lobby-1-12"
          }
          tab_list = "GLOBAL_PING"
          bind_local_address = true
          ping_passthrough = false
          query_enabled = true
          query_port = 25565
          proxy_protocol = false
        }
      ]
      prevent_proxy_connections = false
      player_limit = -1
      online_mode = true
      log_commands = false
      disabled_commands = []
      connection_throttle = 4000
    })
  }
}

resource random_pet proxy {
  keepers = {
    configmap = base64encode(jsonencode(kubernetes_config_map.proxy-config.data))
    plugins = join(",", local.proxy_plugin_urls)
  }
  prefix = "mc-proxy"
  separator = "-"
}

resource kubernetes_daemonset proxy {
  metadata {
    name = random_pet.proxy.id
    namespace = kubernetes_namespace.minecraft.metadata.0.name
    labels = {
      k8s-app = "minecraft-proxy"
    }
    annotations = {}
  }
  spec {
    selector {
      match_labels = {
        k8s-app = "minecraft-proxy"
      }
    }
    template {
      metadata {
        labels = {
          k8s-app = "minecraft-proxy"
        }
      }
      spec {
        host_network = true
        dns_policy = "ClusterFirstWithHostNet"
        toleration {
          key = "ingress"
          value = "true"
          operator = "Equal"
        }
        affinity {
          node_affinity {
            required_during_scheduling_ignored_during_execution {
              node_selector_term {
                match_expressions {
                  key = "ingress"
                  values = ["true"]
                  operator = "In"
                }
              }
            }
          }
        }
        termination_grace_period_seconds = 30
        container {
          name = "proxy"
          image = "itzg/bungeecord"
          stdin = false
          tty = false
          env {
            name = "TYPE"
            value = "WATERFALL"
          }
          env {
            name = "PLUGINS"
            value = join(",", local.proxy_plugin_urls)
          }
          env {
            name = "BUNGEE_JOB_ID"
            value = "340"
          }
          port {
            container_port = 25565
            host_port = 25565
            protocol = "TCP"
            name = "minecraft"
          }
          env {
            name = "MEMORY"
            value = "256M"
          }
          resources {
            requests {
              cpu = "100m"
              memory = "256Mi"
            }
            limits {
              memory = "400Mi"
            }
          }
          volume_mount {
            mount_path = "/config/config.yml"
            name = "proxy-config"
            sub_path = "config.yml"
          }
          env {
            name = "POD_NAME"
            value_from {
              field_ref {
                field_path = "metadata.name"
              }
            }
          }
          env {
            name = "REPLACE_ENV_VARIABLES"
            value = "TRUE"
          }
          env {
            name = "ENV_VARIABLE_PREFIX"
            value = ""
          }
        }
        volume {
          name = "proxy-config"
          config_map {
            name = kubernetes_config_map.proxy-config.metadata.0.name
            default_mode = "0777"
          }
        }
      }
    }
  }
}